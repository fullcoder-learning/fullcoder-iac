======
Docker
======

.. image:: /logos/docker-logo.png
    :scale: 25%
    :alt: Logo Docker
    :align: center

.. |date| date::
.. |time| date:: %H:%M

Ãšltima edición el día |date| a las |time|. 

Esta es la documentación que he recopilado para instalar y configurar docker en sistemas linux.
 
.. contents:: Índice
 
Instalar y configurar Docker  
############################

Para realizar la instalación de docker en nuestra máquina seguimos estos pasos:
- Instalar motor de docker: ``sudo apt install docker.io``
- Instalar docker desktop (para uso local): https://docs.docker.com/engine/install/

Configurar permisos para no usar sudo 
*************************************

Para evitar tener que utilizar sudo en todos los comandos hacemos lo siguiente:

1. Creamos un nuevo grupo docker: ``sudo groupadd docker`` (puede que ya se genere con la instalación)
2. Añadimos nuestro usuario al grupo docker: ``sudo usermod -aG docker $USER``
3. Reiniciamos el pc para arrancar con los cambios  para no tener que usar **sudo** en docker.

Configurar logs
***************

Para evitar acabar con toda la capacidad de disco a base de logs configuramos su rotación:

1. Creamos un archivo **daemon.json**: ``sudo nano /etc/docker/daemon.json`` y editamos:

.. code-block:: json 
    :linenos:

    {
        "log-driver": "json-file",
        "log-opts": {
            "max-size": "10m",
            "max-file": "3"
        }
    }

2. Reiniciamos docker: ``sudo systemctl restart docker``
 
Ejecutar docker 
***************

- Para ejecutar docker en un terminal vez: ``dockerd``
- Para ejecutar docker mientras esta el equipo activo: ``systemctl start docker``
- Para ejecutarlo siempre al iniciar el sistema: ``systemctl enable docker`` (o disable para quitarlo del arranque)

- Probar si esta funcionando: ``sudo docker info`` o ``sudo docker --version``

Docker HUB
**********

Docker hub es un repositorio donde existen un montón de imágenes ya creadas por empresas o particulares en https://hub.docker.com/

Podemos bajarnos diferentes versiones de imágenes con pull: ``docker pull ubuntu:trusty`` esto nos bajará una versión diferente del sistema. Este comando se diferencia de run en que baja las imágenes pero no crea los contenedores como lo haría run.

Estas tags como __trusty__ podemos verlas en Docker Hub accediendo a la imagen que queremos recuperar.

Crear cuenta con Docker HUB
***************************
Creamos una cuenta en Docker para poder subir nuestra imágenes al repositorio público.

- Los repositorios con un nombre único son los repositorios oficiales de dicha tecnologías. Los que preceden un nombre y luego una barra son terceros por ejemplo python para centos: centos/python35

Para crear un repositorio pinchamos en create reopository y ponemos el nombre y la visibilidad.

Contenedores
############

Comandos básicos
****************

- Crear contenedor: ``docker run hello-world``
    - Crear contenedor y abrirlo automáticamente: ``docker run -it ubuntu``
    - Crear contenedor y ejecutar en segundo plano: ``docker run -d nginx``
- Mostrar contenedores arrancados: ``docker ps``
    - Mostrar también los parados: ``docker ps -a``
    - Mostrar último contenedor ejecutado o editado: ``docker ps -l``
    - Mostrar el espacio de todos los contenedores: ``docker ps -a -s``
    - Mostrar una cantidad de resultados: ``docker ps -a -n 3``
- Arrancar contenedor y abrir: ``docker start -i 2351`` (4 primeros digitos del CONTAINER ID)
- Ejecutar contenedor ya arrancado: ``docker exec -it f2ad9cdcc bash`` (usar CONTAINER ID y por último el programa con el que ejecutar "bash, python")
- Salir de contenedor: ``exit``
- Matar un contenedor en segundo plano: ``docker kill 5e683ed457267``
- Borrar contenedor usando su CONTAINER ID: ``docker rm 4703`` (4 primeros digitos)
- Borrar contenedor usando su NAME: ``docker rm hello-world``
- Borrar todos los contenedores que están parados: ``docker system prune --all``

.. attention::
    Al crear un nuevo contenedor, docker buscará si existe imagen en local, luego lo hará en 
    repositorio y si no existe creará una nueva.

.. attention::
    No se pueden borrar imágenes que tengan vinculados contenedores, 
    para ello hay que borrar el contenedor primero. Para forzar borrado utilizar el flag ``-f``

Comandos de gestión 
*******************
- Comprobar actividad de un contenedor: ``docker logs 5e68`` (4 primeros digitos o todos de CONTAINER ID)
    - Ver las últimas x lineas con: ``docker logs 5e68 --tail 10``
- Ver detalles de un contenedor: ``docker stats 5583``
- Comprobar comando que mas recursos gasta: ``docker top 5583``
- Ver caracterÃ­sticas de un contenedor: ``docker inspect bb51d87fba``
    - Guardar caracterÃ­stcas en un json: ``docker inspect bb51d87fba > caracteristicas.json``


Imágenes
########

Comandos básicos 
****************

- Listar imagenes locales de contenedores: ``docker images``
- Borrar imagen mediante su ID: ``docker rmi f2ad9cdcc``
- Ver caracterÃ­sticas de una imagen: ``docker inspect bb51d87fba``
    - Guardar caracterÃ­stcas en un json: ``docker inspect bb51d87fba > caracteristicas.json``
    

Redes en Docker 
###############
Para poder acceder a las aplicaciones de los contenedores docker dispone de puertos privados. Tenemos que hacer dichos puertos públicos y mapearlos en el host.

Si tenemos un TOMCAT en un contenedor podemos mapear el puerto 8080 del contenedor con el puerto 80 del servidor, de modo que se podrá acceder al tomcat desde otro dispositivo.

- Ver las redes disponibles: ``docker network ls``

- bridge es la red que utilizan de manera predefinida los contenedores.
- host: esta red solo trabajan con el host principal, no se pueden ver entre si.
- none: es un contenedor que no tiene red. 

Las redes de tipo bridge asignan una ip a cada contenedor. 

Comandos de red
***************

- Ver que ip tiene un contenedor: ``docker inspect nginx2 | grep IPAd``
- Ver la información de una red: ``docker network inspect bridge > bridge.txt``
- Ver las redes disponibles: ``docker network ls``
- Crear red: ``docker network create red1`` 
    - Crear red y definir el rango en la nueva red: ``docker network create --subnet=192.168.0.0/16 red2``


.. attention::
    Docker recomienda que crees tus propias redes si los contenedores van a relacionarse entre si

Gestionar puertos
*****************

- Averiguar que puerto utiliza un contenedor: ``docker port 4279`` (4 primeros digitos de CONTAINER ID)
- Ejecutar contenedor con puertos públicos: ``docker run -d -P nginx``
    - Utilizar el puerto que queramos: ``docker run -d --name nginx2 -p 8080:80 nginx``

.. attention::
    Hay que definir un nombre de contenedor cuando personalizamos el mapeo del puerto.

Asociar contenedores a una nueva red
************************************

- Asociar contenedor ubuntu a la red2: ``docker network connect red2 ubuntua``

Prueba con contenedores 
#######################

Probar a montar un wordpress con mysql
**************************************

Vamos a conectar un wordpress con Mysql:

1. Creamos una red nueva para que puedan comunicarse: ``docker network create wp_red``
2. Creamos el contenedor mariadb: ``
   
    .. code-block:: bash 
        :lineos:

        docker run -d --name wordpress-db \
            --mount source=wordpress-db,target=/var/lib/mysql \
            -e MYSQL_ROOT_PASSWORD=secret \
            -e MYSQL_DATABASE=wordpress \
            -e MYSQL_USER=manager \
            -e MYSQL_PASSWORD=secret mariadb:10.3.9

3. Creamos una carpeta wordpress, accedemos a la carpeta y creamos una carpeta llamada target.
4. Desde la carpeta de wordpress ejecutamos el comando para preparar wordpress y asociarlo a la base de datos:

    .. code-bock:: bash 
        :linenos:

        docker run -d --name wordpress \
            --link wordpress-db:mysql \
            --mount type=bind,source="$(pwd)"/target,target=/var/www/html \
            -e WORDPRESS_DB_USER=manager \
            -e WORDPRESS_DB_PASSWORD=secret \
            -p 8080:80 \
            wordpress:4.9.8

Si ejecutamos en el navegador localhost:8080 nos abrirá la instalación de wordpress.

Docketizar una aplicación  
*************************



