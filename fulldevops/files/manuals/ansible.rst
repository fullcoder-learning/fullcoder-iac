=======
Ansible 
=======
  
.. image:: /logos/ansible-logo.png
    :scale: 20%
    :alt: Logo Ansible
    :align: center

.. |date| date::
.. |time| date:: %H:%M
 

Última edición el día |date| a las |time|.

Esta es la documentación que he recopilado para trabajar con Ansible, la herramienta devops de automatizaciones.
.. contents:: Índice
 
Primeros pasos
##############

Arquitectura y componentes 
**************************

* Control nodes: servidores desde donde se ejecuta ansible (servidores linux por defecto o WSL en Windows server)
* Manages Nodes: Son los serviores que se van a gestionar por ansible.
* Inventario: Son los ficheros donde se incluyen los nodos gestionados. Se generan manual o dinámicamente.
* Playbooks y plays: Un play ejecuta una serie de tareas en los nodos gestionados. Son formato YAML declarativos y de fácil utilización y mantenimiento.
* Módulos: son scripts independientes que se pueden ejecutar dentro de un playbook.
* Colecciones: son un formato de distribución que puede estar compuesto por playbooks, modulos, roles o plugins. 

Instalación
***********

Desde el gestor de paquetes instalamos lo siguiente:

* Software properties common: ``sudo apt install software-properties-common``
* Añadir repositorio ansible: ``sudo apt-add-repository ppa:ansible/ansible`` 
* Actualizar repositorios: ``sudo apt update``
* Ansible: ``sudo apt install ansible``

Desde pip:

* Instalación global de ansible: ``pip3 install ansible``

Comandos útiles
***************

* ``ansible localhost -m ping``: comprueba si el servidor seleccionado da respuesta. Sino habrá que configurar.
* ``ansible localhost -a "hostname"``: permite ejecutar comandos linux en el servidor. Para eso tenemos que poseer un inventario.

Preparar proyecto
*****************

Requisitos:

* Tener maquinas remotas o virtuales instaladas.
* Python 3
* Acceso SSH 
* Crear usuario con permisos SUDO para no utilizar ROOT.
* Copiar claves SSH del usuario con el que vamos a trabajar.

Configurar SSH 
--------------

- Crear acceso ssh: ``ssh-keygen``
- Accedemos a la carpeta ssh: ``cd .ssh/``
- comprobar que se ha generado la clave: ``cat id_rsa.pub``
- Desde nuestra máquina copiamos la clave ssh a la maquina remota: ``ssh-copy-id 192.168.1.7`` (decimos yes al fingerprint y escribimos el password de la máquina remota)

Una vez hecho esto ya no va a pedir password para conectarnos a la máquina remota vía ssh.

.. attention::
    Asegurate de tener creado un usuario con el mismo nombre en el servidor remoto y no debe pertenecer al grupo sudo para poder hacer gestiones, una vez creado se le añaden los privilegios con visudo en el apartado acceso sin contraseña.

Crear usuario ansible y configurar SSH 
--------------------------------------

Es necesario crear un usuario ansible para que ansible pueda ejecutar operaciones:

- creamos en nuestra máquina un usuario ansible: ``sudo adduser ansible``
- creamos la contraseña del usuario ansible: ``sudo passwd ansible`` (si lo requiere)
- comprobamos si se ha creado el directorio del usuario: ``ls /home/ansible``

Repetimos los pasos en la máquina de destino:

- accedemos a la máquina externa y creamos un usuario ansible: ``sudo adduser ansible``
- creamos la contraseña del usuario ansible: ``sudo passwd ansible`` (si lo requiere)
- comprobamos si se ha creado el directorio del usuario: ``ls /home/ansible``

Ahora vamos a crear una clave ssh:

- Accedemos a nuestro usuario ansible: ``su ansible``
- Ejecutamos el comando: ``ssh-keygen``
- Copiamos la clave al servidor remoto: ``ssh-copy-id 192.168.1.7``
- Accedemos al servidor remoto y editamos el fichero sudoers: ``sudo nano /etc/sudoers``
- debajo de la línea ``root    ALL=(ALL:ALL) ALL`` añadimos la siguiente línea: ``ansible ALL=(ALL) ALL`` 

Ahora debemos hacer lo mismo con el usuario de nuestra máquina para que pueda conectarse usando la contraseña automáticamente.

.. important::
    Si usamos el comando useradd creara un usuario pero no su directorio, es mejor utilizar en su lugar adduser. 


Configurar acceso sin password 
------------------------------

Para que ansible pueda ejecutar tareas sudo le va a pedir password el sistema, debemos configurar sudoers para que no pida password:

- Accedemos al servidor remoto y editamos el fichero sudoers: ``sudo visudo``
- debajo de la línea ``root    ALL=(ALL:ALL) ALL`` añadimos la siguiente línea: ``ansible ALL=(ALL) NOPASSWD:ALL`` 

.. important::
    No debemos añadir el usuario al grupo sudo ni root o no funcionará.

.. important::
    Si se trabaja desde el usuario ansible no debe haber problemas de permisos. En caso contrario es posible que se deba escalar permisos.

Comando ad-hoc
##############

El comando para ejecutar ad-hoc es el propio ``ansible``

Crear inventario
****************
El inventario es la lista de máquinas con las que vamos a trabajar:

* Creamos un directorio para el proyecto: ``mkdir pruebadevops``
* Creamos un archivo para nuestro inventario: ``code maquinas`` y añadimos linea a linea todas las ip de cada maquina o nombre dns (url) externa.
* Probamos a ejecutar un comando ad-hoc contra todas las máquinas el comando linux ``ping``: ``ansible -i maquinas all -m ping``. Con -i hacemos referencia al flag inventory

Reemplazar ip por nombres en inventario
---------------------------------------

* Editamos el archivo hosts de nuestro equipo: ``sudo nano /etc/hosts`` y añadimos al final nuestra máquina con su ip y un nombre identificativo **hermes** (tal y como aparece localhost y nuestra máquina)
* Editamos el archivo **maquinas** y cambiamos las ips que pusimos por los nombres de cada máquina.
* Ahora al ejecutar el comando ansible no es necesario poner la ip, sino el nombre de la máquina **hermes**: ``ansible -i maquinas hermes -m ping``


Comandos ad-hoc
***************

Los comandos ad-hoc sirven para hacer ejecuciones de comandos rápidos. Para activar un comando ad-hoc se añade el flag -m (module)

ping
----
El primer módulo que hemos conocido, sirve para comprobar si los servidores remotos dan respuesta:

* Hacer ping a una de las máquinas, ej: **hermes**: ``ansible -i maquinas hermes -m ping``
* Hacer ping a todas las máquinas: ``ansible -i maquinas all -m ping``

.. note::
    la elección de máquina remota se puede aplicar en cualquier comando.

Command
-------
Permite lanzar un comando contra la máquina remota:

* Ejecutar un comando con command y le pasamos el argumento con -a (que es el comando): ``ansible -i maquinas hermes -m command -a "ls -l /"``

Shell
-----
Con este comando podemos ejecutar comandos desde una shell de linux a diferencia de command que nos da una respuesta (muy util para utilizar pipes u otros comandos no soportados por command):

* Ejecutar shell para poder usar pipes: ``ansible -i maquinas hermes -m shell -a "ls -l / | tail -5"``

Setup
-----
Permite recuperar propiedades y características de las máquinas externas:

* Al ejecutar setup nos devuelve mucha información acerca de la máquina: ``ansible -i maquinas hermes -m setup``

Copy
----
Permite copiar ficheros o manipularlos. 

Copiar desde local a remoto:

* Creamos un fichero dentro de nuestro proyecto llamada prueba: ``touch prueba.txt``
* Copiamos archivo en nuestra máquina externa: ``ansible -i maquinas hermes -m copy -a "src=prueba.txt dest=/tmp/prueba.txt"``
* Copiar archivo modificando permisos: ``ansible -i maquinas hermes -m copy -a "src=prueba.txt dest=/tmp/prueba.txt" mode=777``
* Si vamos al servidor remoto veremos el archivo copiado.

.. note::
    podemos copiar directorios completos sin tener que añadir ninguna otra instrucción. Tampoco es necesario añadir en dest el nombre del directorio.

Yum 
---
Permite hacer instalaciones en entornos tipo red-hat, fedora, centos. Para otros no se puede utilizar:

* Instalar apache con yum: ``ansible -i maquinas hermes -m yum -a "name=httpd state=present"``

Pero esto puede dar error según nuestro permisos, para ello tenemos que escalar a utilizar el usuario ansible:

* Instalar utilizando el usuario ansible: ``ansible -i maquinas hermes --become --ask-become-pass -m yum -a "name=httpd state=present"``

Apt
---
Permite instalar en sistemas como debian, ubuntu, etc:

* Instalar apache con apt: ``ansible -i maquinas hermes -m apt -a "name=apache2 state=present"``

Pero esto puede dar error según nuestro permisos, para ello tenemos que escalar a utilizar el usuario ansible:

* Instalar utilizando el usuario ansible: ``ansible -i maquinas hermes --become --ask-become-pass -m apt -a "name=apache2 state=present"``

Service
-------
Permite arrancar servicios con el comando nativo de linux service:

* Ejecutar apache en los servicios: ``ansible -i maquinas hermes --become --ask-become-pass -m service -a "name=httpd state=started"``

.. attention::
    las instrucciones --become y --ask-become-pass lo utilizamos en caso de que no nos de acceso con el usuario que estamos utilizando en local. Con estos comandos se escalan permisos y se pide el password.

Inventarios y configuraciones
#############################

Fichero de configuración  
************************

El archivo de configuración se llama **ansible.cfg**.

* Crear archivo **ansible.cfg** a nivel de fichero en la carpeta del proyecto: ``ansible-config init --disable > ansible.cfg``  

En este archivo de configuración podemos configurar la ruta de los inventarios o el usuario become.

tambien podemos crearlo el archivo **ansible.cfg** a mano en el proyecto y editarlo:

.. code-block::
    :linenos:

    [defaults]

    inventory=./maquinas

.. note::
    otros sitios donde podemos alojar este archivo es en una variable de entorno **ANSIBLE_CONFIG** de linux, el directorio home del usuario o en /etc/ansible/ 

Inventarios 
***********

Los inventarios son los archivos donde se indican los servidores con los que vamos a trabajar.

A la hora de ejecutar inventarios podemos hacerlo con varios siempre respetando el orden en el que se llaman.

definimos donde se va a encontrar nuestro inventario. Ahora podemos agrupar nuestras máquinas por nombres:

.. code-block::
    :linenos:

    [centos]
    hermes
    zeus
    ephestos

    [ubuntu]
    caronte
    sefiroth

Ahora tenemos tres formas de interactuar con las máquinas:
* Maquina a maquina: ``ansible hermes -m ping``
* Por grupos de maquinas: ``ansible centos -m ping``
* Todas las maquinas: ``ansible all -m ping``

Y como vemos ya no es necesario escribir el nombre del inventario.

.. note::
    se recomienda instalar la extension de ansible si utilizamos visual studio code


Inventarios en YAML 
*******************

podemos crear un inventario en formato yaml mas escalable **maquinas.yaml**:

.. code-block::
    :linenos:

    all:
        hosts:
            maquinasuelta:
        children:
            centos:
            hosts:
                hermes:
                zeus:
                poseidon:
            ubuntu:
            hosts:
                caronte:
                sefiroth:

Cambiamos en **ansible.cfg** el nombre del nuevo inventario:

.. code-block:: 
    :linenos:

    [defaults]

    inventory=./maquinas.yaml

Todo es similar salvo que para ejecutar una maquina sin agrupar **maquinasuelta** se ejecuta: ``ansible unbrouped -m ping``

Playbooks 
#########

Los playbooks son plantillas que nos permiten ejecutar entornos complejos dentro de ansible sin necesidad de intervención humana automatizando multiples procesos en una sola tarea.

Cada playbook tiene una serie de plays con tareas específicas como montar el servidor o desplegar una aplicación.

Para trabajar con ansible disponemos de módulos como los que hemos visto (ping, copy, setup) y muchos mas en la documentación de ansible.

playbook
********

En nuestro proyecto creamos un archivo llamado **ping.yaml** y editamos:

.. code-block:: yaml 
    :linenos:

    ---
    - name: Primer play del playbook
    hosts: all # puedes poner una maquina o grupo como hermes

    tasks:
    - name: Hacer un ping 
        ansible.builtin.ping:

Un playbook se inicia siempre con tres guiones (---) y luego cada play se inicia con un - name y sus propiedades. Dentro de este play añadimos tasks (tareas)

El playbook se ejecuta con el comando: ``ansible-playbook ping.yaml`` 

Añadir otra tarea al play
+++++++++++++++++++++++++

.. code-block:: yaml 
    :linenos:

    ---
    - name: Primer play del playbook
    hosts: hermes

    tasks:
    - name: Hacer un ping 
        ansible.builtin.ping:
    - name: Crear un fichero 
        ansible.builtin.shell:
        touch /tmp/prueba.txt

Añadir otro play al playbook 
++++++++++++++++++++++++++++

Vamos a cambiar el apache por un nginx:

.. code-block:: yaml 
    :linenos:

    ---
    - name: Primer play del playbook
    hosts: all

    tasks:
    - name: Hacer un ping 
        ansible.builtin.ping:
    - name: Crear un fichero 
        ansible.builtin.shell:
        touch /tmp/prueba.txt

    - name: Cambiar apache por nginx
    hosts: all 
    become: true # para ejecutar comandos de instalación y ejecución de servicios hay que escalar permisos
    become_user: root
    become_method: sudo


    tasks:
    - name: Parar apache
        ansible.builtin.service:
        name: httpd
        state: stopped
    - name: Instalar EPEL software repository
        ansible.builtin.yum:
        name: epel-release
        state: present
        update_cache: true
    - name: Instalar nginx 
        ansible.builtin.yum:
        name: nginx
        state: present
        update_cache: true # esta opción limpia la cache 
    - name: Arrancar nginx
        ansible.builtin.service:
        name: nginx 
        state: started


.. attention::
    En caso de tener errores al ejecutar comandos con privilegios añadimos privilegios sudo al usuario ansible en maquina local y maquinas remotas con los comandos: ``sudo usermod -aG wheel ansible`` para centos y para ubuntu: ``sudo usermod -aG sudo ansible``

Visualizar mas información
++++++++++++++++++++++++++

Se puede visualizar mas información de la ejecución de ansible-playbook con -v:

- v: visualiza datos de salida 
- vv: visualiza datos de salida y entrada 
- vvv: Información sobre conexiones 
- vvvv: Añade toda la información disponible

Su ejecución sería: ``ansible-playbook ping.yaml -vvvv``

Copiar un fichero para el servidor nginx 
****************************************

1. Creamos un fichero en nuestro proyecto llamado **index.html**:

.. code-block:: html 
    :linenos:

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
    </head>
    <body>
        <h1>Título del proyecto</h1>

        <p>Esto es un proyecto ansible</p>
    </body>
    </html>

2. Cargamos el archivo index: 

.. code-block:: yaml 
    :linenos:

    ---
    - name: Primer play del playbook
    hosts: all

    tasks:
    - name: Hacer un ping 
        ansible.builtin.ping:
    - name: Crear un fichero 
        ansible.builtin.shell:
        touch /tmp/prueba.txt

    - name: Cambiar apache por nginx
    hosts: all 
    become: true # para ejecutar comandos de instalación y ejecución de servicios hay que escalar permisos
    become_user: root
    become_method: sudo

    tasks:
    - name: Parar apache
        ansible.builtin.service:
        name: httpd
        state: stopped
    - name: Instalar EPEL software repository
        ansible.builtin.yum:
        name: epel-release
        state: present
        update_cache: true
    - name: Instalar nginx 
        ansible.builtin.yum:
        name: nginx
        state: present
        update_cache: true 
    - name: Arrancar nginx
        ansible.builtin.service:
        name: nginx 
        state: started
    # copiar indice:
    - name: Copiar index para nginx 
        ansible.builtin.copy:
        src: ./index.html
        dest: /var/www/html/index.html
        owner: root 
        group: root 
        mode: '0644'

Nos vamos a http://hermes (o la ip del servidor remoto)

.. note:: 
    Es probable que no funcione el servidor cuando lo ejecutemos debido a que falte configurar nginx. En ese caso quitamos la parte de nginx y arrancamos httpd (apache 2)

Externalizar tareas
*******************

Vamos a crear tareas en archivos yaml para poder reutilizar en nuestros plays: 

* Crear archivo **parar_apache.yaml**:

.. code-block:: yaml 
    :linenos:

    - name: Parar servidor apache Centos
    ansible.builtin.service:
        name: httpd
        state: stopped

* Cargar tarea en el playbook:

.. code-block:: yaml 
    :linenos:

    ---
    - name: Primer play del playbook
    hosts: all

    tasks:
    - name: Hacer un ping 
        ansible.builtin.ping:
    - name: Crear un fichero 
        ansible.builtin.shell:
        touch /tmp/prueba.txt

    - name: Cambiar apache por nginx
    hosts: all 
    become: true 
    become_user: root
    become_method: sudo


    tasks:
    - name: Parar apache
        include_tasks: parar_apache.yaml # añadimos la tarea externa al play quitando la tarea de parar apache original
    - name: Instalar EPEL software repository
        ansible.builtin.yum:
        name: epel-release
        state: present
        update_cache: true
    - name: Instalar nginx 
        ansible.builtin.yum:
        name: nginx
        state: present
        update_cache: true 
    - name: Arrancar nginx
        ansible.builtin.service:
        name: nginx 
        state: started
    # copiar indice:
    - name: Copiar index para nginx 
        ansible.builtin.copy:
        src: ./index.html
        dest: /var/www/html/index.html
        owner: root 
        group: root 
        mode: '0644'

Variables 
#########

Variables de entorno 
********************

Definene valores en el sistema operativo, por ejemplo ANSIBLE_CONFIG 

Variables de inventario 
***********************

Se incluyen en los ficheros de inventarios y se pueden aplicar a nivel de hosts o de grupo, estas las heredan los playbooks. Se edita el inventario **maquinas.yaml**:

.. code-block:: yaml 
    :linenos:

    all:
    hosts:
        maquinasuelta:
    children:
        centos:
        hosts:
            hermes: # añadimos identando las variables que queremos cargar con el servidor
                puerto: 5000
                entorno: desarrollo
            zeus:
            poseidon:
        ubuntu:
        hosts:
            caronte:
            sefiroth:

Ficheros de variables
*********************

Las variables se pueden guardar en ficheros para ser reutilizadas:

* Para las variables de cada host, creamos un directorio específico llamado **host_vars** y creamos los ficheros con el nombre del host por ejemplo **hermes.yaml**:

.. code-block:: yaml
    :linenos:

    ansible_user: ggg

* Para los grupos de hosts, creamos un directorio llamado **group_vars** y los archivos con el nombre de cada grupo de hosts **centos**:

.. code-block:: yaml
    :linenos:

    ansible_user: pepe

Variables de Playbook 
*********************

Se asocian a nivel de play o tareas y son las mas utilizadas.

Añadir variables al playbook, creamos un nuevo playbook llamado por ejemplo **variables.yaml**:

.. code-block:: yaml 
    :linenos:

    ---
    - name: Probando variables
    hosts: hermes
    
    vars: # añadimos las variables aquí:
        - mensaje: Mensaje de prueba 
        - array_de_entornos:
        - desarrollo
        - testing
        - produccion
        - diccionario_de_desarrollo: # requiere de mas tabulación sino falla:
            tipo: linux
            memoria: 4GB
            disco: 500GB

    # en la tarea pasamos el mensaje para que se vea por el debugger:
    tasks:
        - name: Imprimir variables
        debug: # recuperar variable y pasarlo por la cadena:
            msg: "Imrimiendo: {{mensaje}}"

        - name: Imprimir array 
        debug:
            msg: "Imprimir todo el array: {{array_de_entornos}}, imprimir solo uno: {{array_de_entornos[1]}}, imprimir un rango: {{array_de_entornos[0:2]}}"

        - name: Imprimir diccionario 
        debug:
            msg: "Imprimir todo el diccionario: {{diccionario_de_desarrollo}}, imprimir solo uno:{{diccionario_de_desarrollo['tipo']}}"

        - name: Imprimir variable local de task 
        vars: # variables locales de las tasks
            tarea: Impresion de variables 
        debug: 
            msg: "Imprimir: {{tarea}}"
        
        # Intentar imprmir variable local de otra tarea en una nueva:
        - name: Intentar imprmir variable de otra tarea 
        debug: 
            msg: "Imprimir: {{tarea}}"

Ejecutar el playbook: ``ansible-playbook variables.yaml``

Variables desde ficheros 
************************

* Creamos una carpeta **resources** en el proyecto y dentro un archivo para nuestras variables **pruebavars.yaml**:

.. code-block:: yaml 
    :linenos:

    nombre: Guillermo 
    apellidos: Granados

* Ahora para poder utilizar esta variable cargamos el archivo en el playbook que queramos: 

.. code-block:: yaml 
    :linenos:

    ---
    - name: Probando variables
    hosts: hermes
    # cargamos el archivo de variables (se puede cargar cada uno con su ruta después del guión):
    vars_files:
        - resources/pruebavars.yaml

    tasks:
        - name: Imprimir nombre
        debug: 
            msg: "Me llamo {{nombre}} {{apellidos}}"


Variables desde linea de comandos 
*********************************

Es posible cargar variables desde linea de comandos al playbook:

.. code-block:: yaml 
    :linenos:

    ---
    - name: Probando variables
    hosts: hermes

    tasks:
        - name: Imprimir nombre
        debug: # añades una variables que no existe todavía, pero que inicializamos desde CLI:
            msg: "Has escrito {{palabra}}"

* Ahora ejecutamos el playbook pasandole la variable palabra: ``ansible-playbook variables.yaml --extra-vars "palabra='mensaje desde consola'"``

Variables de roles
******************

Se utilizan dentro de un Role

Variables especiales
******************** 

* Magic: Reflejan el estado interno y no son modificables.
* Facts: continen información de un host, para obtener las variables de tipo fact ejecutamos: ``ansible hermes -m setup``
    * preguntar por uno o varios de los facts: ``ansible hermes -m setup -a 'filter=ansible_system,ansible_os_family,ansible_processor'`` (separar por coma cada una de los facts a recuperar)
    * Uso de facts en playbooks:
        
        .. code-block:: yaml 
            :linenos: 

            ---
            - name: Probando variables
            hosts: hermes

            tasks:
                - name: Imprimir facts
                debug: # Se pueden llamar directamente sin necesidad de importar ya que ansible hace un facts antes de comenzar:
                    msg: "Arquitectura: {{ansible_facts['architecture']}}"


* Conexión: determinan como se ejecutan las acciones contra un target como **ansible_become_user**

Modulos más utilizados  
######################

Módulo ping
***********

Ejecuta un ping en las máquinas remotas:

.. code-block:: yaml 
    :linenos:

    ---
    - name: Primer play del playbook
    hosts: all 

    tasks:
    - name: Hacer un ping 
        ansible.builtin.ping:

Módulo shell 
************

Permite ejecutar un comando bash en las máquinas remotas:

.. code-block:: yaml 
    :linenos:

    ---
    - name: Primer play del playbook
    hosts: hermes

    tasks:
    - name: Crear un fichero 
        ansible.builtin.shell:
        touch /tmp/prueba.txt

Módulo apt/yum 
**************

Permite instalar paquetes yum (red hat) o apt (debian) en las máquinas remotas:

* Ejemplo YUM:

.. code-block:: yaml 
    :linenos:

    ---
    - name: Primer play del playbook
    hosts: hermes

    tasks:
    - name: Instalar nginx 
        ansible.builtin.yum:
        name: nginx
        state: present
        update_cache: true

* Ejemplo APT:

.. code-block:: yaml 
    :linenos:

    ---
    - name: Primer play del playbook
    hosts: hermes

    tasks:
    - name: Instalar nginx 
        ansible.builtin.apt:
        name: nginx
        state: present
        update_cache: true

Módulo Service 
**************

Permite arrancar, parar y reiniciar servicios en la máquina remota:

.. code-block:: yaml 
    :linenos:

    ---
    - name: Primer play del playbook
    hosts: hermes

    tasks:
    - name: Parar apache
        ansible.builtin.service:
        name: httpd
        state: stopped

Módulo Copy 
***********

Copia un fichero en las máquinas remotas:

.. code-block:: yaml 
    :linenos:

    ---
    - name: Primer play del playbook
    hosts: hermes

    tasks:
    - name: Copiar index para nginx 
        ansible.builtin.copy:
        src: ./index.html
        dest: /var/www/html/index.html
        owner: root 
        group: root 
        mode: '0644'

Módulo Debug 
************

Imprime por consola un mensaje:

.. code-block:: yaml 
    :linenos:

    ---
    - name: Primer play del playbook
    hosts: hermes

    tasks:
        - name: Imprimir facts
        debug: # Se pueden llamar directamente sin necesidad de importar ya que ansible hace un facts antes de comenzar:
            msg: "Arquitectura: {{ansible_facts['architecture']}}"

Módulo File 
***********

Crea un fichero en la máquina remota:

.. code-block:: yaml 
    :linenos:

    ---
    - name: Primer play del playbook
        hosts: hermes
        become: true # para cambiar permisos son necesarias las variables become
        become_user: root
        become_method: sudo

        tasks:
        - name: Crear fichero vacío
            file: # crear fichero:
            path: "/tmp/pruebafile.txt"
            state: "touch"

        - name: Borrar fichero vacío 
            file: # borrar fichero:
            path: "/tmp/pruebafile.txt"
            state: "absent"

        - name: Crear fichero vacío para permisos
            file: # crear fichero:
            path: "/tmp/pruebafile.txt"
            state: "touch"

        - name: Cambiar permisos fichero vacío 
            file: # Cambiar permisos fichero:
            path: "/tmp/pruebafile.txt"
            owner: ansible
            group: ansible 
            mode: "0777"

        - name: Crear un directorio
            file: # crear directorio:
            path: "/tmp/carpetaprueba"
            state: directory
            mode: "0775"

Modulo user 
***********

Permite gestionar usuarios en maquina remota:

.. code-block:: yaml 
    :linenos:

    ---
    - name: Usuarios 
        hosts: hermes
        become: true 
        become_user: root
        become_method: sudo

        tasks:
        - name: Crea un usuario nuevo 
            user:
            name: nuevouser



Modulo group 
************

Permite gestionar grupos de usuarios en maquinas remotas: 

.. code-block:: yaml 
    :linenos:

    ---
    - name: Grupos 
        hosts: hermes
        become: true 
        become_user: root
        become_method: sudo

        tasks:
        - name: Crea un grupo nuevo 
            group:
            name: nuevogrupo

* Crear un usuario y añadir grupo:

.. code-block:: yaml 
    :linenos:

    ---
    - name: Trabajar con usuarios 
    hosts: hermes
    become: true 
    become_user: root
    become_method: sudo

    tasks:
    - name: Crear un usuario y grupo
        user:
        name: otrousuario 
        group: nuevogrupo # se debe asignar a un grupo existente
        shell: /bin/bash # asignar la bash con la que trabajara  
        home: /home/usuariohomer # asignar la carpeta del usuario diferente

Módulo lineinfile
*****************

Editar un fichero existente y añadir datos: 

.. code-block:: yaml 
    :linenos:

    ---
    - name: Editar archivo 
    hosts: hermes

    tasks: 
    # añadir una nueva línea la fichero:
    - name: Editar fichero existente
        ansible.builtin.lineinfile:
        path: /tmp/prueba.txt 
        line: nueva linea 

    # reemplazar una línea del fichero:
    - name: Reemplazar línea 
        ansible.builtin.lineinfile:
        path: /tmp/prueba.txt
        search_string: nueva linea 
        line: linea cambiada 

    # añadir nueva línea para reemplazar:
    - name: Editar fichero existente
        ansible.builtin.lineinfile:
        path: /tmp/prueba.txt 
        line: otra línea 

    # borrar línea:  
    - name: Borrar linea 
        ansible.builtin.lineinfile:
        path: /tmp/prueba.txt 
        search_string: linea cambiada 
        state: absent

Modulo set_fact
***************

Permite crear variables dentro de las máquinas host: 

.. code-block:: yaml 
    :linenos:

    ---
    - name: trabajar con facts
    hosts: hermes

    tasks: 
    # crear variable:
    - name: Crear variable fact
        set_fact:
        nuevavar: "esta es una variable de prueba"

    # ver la variable con fact: 
    - name: Ver variable 
        debug:
        msg: "valor de variable: {{nuevavar}}"

Módulo fetch
************

Copia datos de una máquina remota a la máquina host:

.. code-block:: yaml 
    :linenos:

    ---
    - name: Handlers 
    hosts: hermes 

    tasks:
    - name: Copiar index.html 
        ansible.builtin.fetch:
        src: /tmp/pruebafile.txt
        dest: /tmp/fetched # el destino es una carpeta donde se guardara con el nombre de la máquina host y toda la ruta hacia el archivo

El resultado se ha guardado en la ruta local: ``ls /tmp/fetched/hermes/tmp/pruebafile.txt``
    

Estructuras de Control
######################

Condicional WHEN 
****************

WHEN permite ejecutar tareas cuando se cumple una condición:

.. code-block:: yaml 
    :linenos:

    ---
    - name: Condicional WHEN
    hosts: hermes

    tasks: 
    - name: distribución
        debug:
        msg: "{{ansible_facts['distribution']}}"
    - name: Capturar fecha 
        shell:
        cmd: date 
        register: fecha # guardamos el resultado en una variable
        # cuando se cumpla la condición de la distribución se ejecutará esta tarea
        when: ansible_facts['distribution']=="CentOS"
    - name: Imprimir fecha capturada
        debug:
        msg: "{{ fecha }}"

Ignorar errores
***************

Si no queremos que se pare el proceso de ejecución podemos habilitar el ignorar errores en ciertas tareas que no sean cruciales:

.. code-block:: yaml 
    :linenos:

    ---
    - name: Condicional WHEN
    hosts: hermes

    tasks: 
    - name: distribución
        debug:
        msg: "{{ansible_facts['distribution_me_invento_nombre']}}"
        ignore_errors: true # ignoramos el error de no saber la variable
    - name: Capturar fecha 
        shell:
        cmd: date 
        register: fecha
        when: ansible_distribution=="CentOS"
    - name: Imprimir fecha 
        debug:
        msg: "{{ fecha }}"

Ignorar servidores parados 
**************************

Normalmente no ocurre nada si falla algún servidor, pero podemos ignorarlos igualmente para evitar posible fallos:

.. code-block:: yaml 
    :linenos:

    ---
    - name: Condicional WHEN
    hosts: all 
    ignore_unreachable: true # ignorar servidores que no puedan ser ejecutados

    tasks: 
    - name: distribución
        debug:
        msg: "{{ansible_facts['distribution_me_invento_nombre']}}"
        ignore_errors: true
    - name: Capturar fecha 
        shell:
        cmd: date 
        register: fecha
        when: ansible_distribution=="CentOS"
    - name: Imprimir fecha 
        debug:
        msg: "{{ fecha }}"

Determinar cuando debe dar error 
********************************

Podemos determinar cuando una tarea o un play debe de fallar y dejar de ejecutarse:

.. code-block:: yaml 
    :linenos:

    ---
    - name: Condicional WHEN
    hosts: hermes 

    tasks: 
    - name: distribución
        debug:
        msg: "{{ansible_facts['distribution']}}"
        failed_when: ansible_facts['distribution']=="CentOS" # podemos establecer una condición para detener la ejecución de la tarea 
    - name: Capturar fecha 
        shell:
        cmd: date 
        register: fecha
        when: ansible_facts['distribution']=="CentOS"
    - name: Imprimir fecha 
        debug:
        msg: "{{ fecha }}"

Generar ficheros log 
********************

Se puede configurar a nivel global desde **ansible.cfg**:

.. code-block::  
    :linenos:

    [defaults]

    inventory=./maquinas.yaml
    log_path=/tmp/ansible.log 

Esto genera un archivo log en la máquina de control que podemos visualizar con ``cat /tmp/ansible.log``

Bucle loop 
**********

El bucle mas recomendado para trabajar en ansible:

.. code-block:: yaml
    :linenos:

    ---
    - name: Bucle loop
    hosts: hermes 
    vars: 
        nombres:
        - Pedro 
        - Laura
        - Alfredo
        - Ignacio
        - Cristina
        persona:
        nombre: Antonio
        edad: 34 
        profesion: Camionero

    tasks: 
    - name: bucle sencillo
        debug:
        msg: "{{indice}}: {{item}}" # loop crea una variable de interacción automática 
        loop: # asignamos los valores que se recorrerán arriba
        - valor 1
        - valor 2
        - valor 3
        loop_control: # opcionalmente podemos sacar el índice de cada valor
        index_var: indice
    - name: crea varios usuarios 
        become: true # para ejecutar comandos de instalación y ejecución de servicios hay que escalar permisos
        become_user: root
        become_method: sudo
        ansible.builtin.user:
        name: "{{ item }}"
        state: present
        loop: # establecemos los nombres de usuarios:
        - usuarioprueba1
        - usuarioprueba2
        - usuarioprueba3
    # recorrer un array con loop:
    - name: Imprimir mensaje saludo 
        debug:
        msg: "Hola {{ item }}!!"
        loop: "{{ nombres }}"
    # recorrer un diccionario:
    - name: Recorremos un diccionario con datos
        debug:
        msg: "{{ item.key }}: {{ item.value }}" # si usamos solo item podemos ver la estructura de cada propiedad, de modo que podemos usar tanto key como value para recuperar uno de los valores
        loop: "{{ persona | dict2items }}" # con un filtro podemos pasar el diccionario a items

    

Bucle with_items
****************

Es identico a loop salvo por alguna diferencias en la salida de datos:

.. code-block:: yaml
    :linenos:

    ---
    - name: Bucle loop
    hosts: hermes 

    tasks: 
    - name: bucle sencillo
        debug:
        msg: "{{item}}"
        with_items: # with_items funciona exactamente igual
        - valor 1
        - valor 2
        - valor 3

Handlers
********

Es una tarea que se va a ejecutar porque ha sido invocada por otra: 

.. code-block:: yaml
    :linenos:

    ---
    - name: Handlers 
    hosts: hermes 
    become: true # para ejecutar comandos de instalación y ejecución de servicios hay que escalar permisos
    become_user: root
    become_method: sudo

    tasks:
    - name: Copiar index.html 
        ansible.builtin.copy:
        src: index.html
        dest: /var/www/html 
        # disparamos el handler:
        notify:
        - lanzar_mensaje
        - reiniciar_apache

    # Declarar handlers:
    handlers:
    - name: lanzar_mensaje
        debug:
        msg: "Mensaje de prueba"
    - name: reiniciar_apache 
        ansible.builtin.service:
        name: httpd
        state: restarted 

.. attention::
    Los handlers solo se ejecutan cuando las tareas realizan algún tipo de cambio o cualquier cosa que retorne una respuesta. En el caso de copiar un fichero ya existente no funcionará, para que funcione hay que borrar el fichero.


Control de errores 
******************

Podemos ejecutar un bloque rescue en caso de que no pueda ejecutar algunas tareas:

.. code-block:: yaml
    :linenos:

    ---
    - name: Handlers 
    hosts: hermes 
    vars:
        archivo: pruebafile1.txt
        
    tasks:
        - name: Ejemplo control
        block: # se crea un bloque de control
        - name: Copiar archivo 
            ansible.builtin.fetch:
            src: "/tmp/{{ archivo }}" # archivo no existente
            dest: /tmp/fetched

        # RESCUE:
        rescue:
        - name: Mensaje de error
            ansible.builtin.debug:
            msg: "No se pudo copiar el archivo {{ archivo }}"

Filtros 
#######

Ver tipo de dato de una variable 
********************************

.. code-block:: yaml
    :linenos:

    ---
    - name: Filtros 
    hosts: hermes 
    vars:
        nombre: Guillermo
        edad: 36

    tasks:
        - name: Visualizar tipo de variable
        debug:
            msg: "Nombre: {{nombre | type_debug }}, edad: {{edad | type_debug }}"

Conversiones
************

.. code-block:: yaml
    :linenos:

    ---
    - name: Filtros 
    hosts: hermes 
    vars:
        nombre: Guillermo
        edad: 36

    tasks:
        - name: Convertir número a cadena
        debug:
            var: edad | string

Cadenas 
*******

.. code-block:: yaml
    :linenos:

    ---
    - name: Filtros 
    hosts: hermes 
    vars:
        nombre: Guillermo

    tasks:
        - name: Convertir a mayúscula
        debug:
            var: nombre | upper

        - name: Convertir a minúsculas
        debug:
            var: nombre | lower

        - name: Convertir a mayúscula (distingue mayusculas de minúsculas)
        debug:
            var: nombre | replace("G", "W")

        - name: Tamaño de cadena
        debug:
            var: nombre | length

        - name: Usar varios filtros
        debug:
            var: nombre | upper | replace("U", "X")

Números
*******

.. code-block:: yaml
    :linenos:

    ---
    - name: Filtros 
    hosts: hermes 
    vars:
        edad: 36
        peso: 97.82

    tasks:
        - name: Elevar a potencia
        debug:
            var: edad | pow(2)

        - name: Raiz cuadrada 
        debug: 
            var: edad | root()

        - name: Redondeo 
        debug: 
            var: peso | round()
        
        - name: Numero aleatorio 
        debug: 
            var: edad | random


Listas
******

.. code-block:: yaml
    :linenos:

    ---
    - name: Filtros 
    hosts: hermes 
    vars:
        lista_numeros:
        - 4
        - 11
        - 2
        - 17
        - 18
        - 10
        lista_cadena:
        - Megadrive
        - Master system
        - Atari 2600
        - Playstation
        cadena_cosas: Nocilla, Galletas, Platanos, Patatas

    tasks:
        - name: Número menor
        debug:
            msg: "{{lista_numeros | min}}"

        - name: Número mayor
        debug:
            msg: "{{lista_numeros | max}}"

        - name: Orden alfabético primero y último
        debug:
            msg: "Primero: {{lista_cadena | min}}, último: {{lista_cadena | max}}"

        - name: Unir elementos de la lista
        debug:
            msg: "{{lista_cadena | join(',')}}"

        - name: Crea lista a partir de una cadena
        debug:
            msg: "{{cadena_cosas | split(', ')}}"

Roles 
#####

Los roles contienen variables, archivos, tareas, controladores y otros artefactos de Ansible.

Directorios de roles:
* Default: variables predefinidas para el rol y tienen un nivel de prioridad bajo.
* Vars: variables para los roles de ansible que tienen una prioridad más alta que las predeterminadas en default.
* Tasks: Tareas ejecutadas por un rol específico.
* Files: Archivos que el usuario quiere copiar en el host remoto.
* Handlers: handlers que pueden ser invocados por las tareas.
* Templates: plantillas a utilizar.
* Meta: metadatos del rol como el autor o las dependencias.
* Tests: Para realizar pruebas con el role.

Crear estructura de roles
*************************

Crear estructura de roles con el comando: ``ansible-galaxy init mariadb``. Siendo mariadb el nombre de la carpeta de roles para instalar un servidor apache.

Para normalizarlo y declararlo es necesario acceder al directorio meta y modificar el archivo main.yml 

Tareas en roles
***************

Tras ejecutar el comando init vamos a crear las tareas para instalar mariadb, para ello se edita **tasks/main.yml**:

.. code-block:: yaml 
    :linenos:

    ---
    # tasks file for mariadb
    - name: Instalar MariaDB 
    ansible.builtin.yum:
        name: mariadb-server
        state: present

    - name: Arrancar MariaDB
    ansible.builtin.service:
        name: mariadb 
        state: started

.. note::
    En un rol nunca ponemos plays ni playbooks. Solo tareas.

Creamos un playbook para probar el rol nuevo **pruebabook.yaml**:

.. code-block:: yaml 
    :linenos:

    ---
    - name: Prueba roles
    hosts: hermes 
    become: true 
    become_user: root
    become_method: sudo

    roles: # cargamos los roles a ejecutar antes de las tareas
        - mariadb
    
    # nos aseguramos de que se ejecuta todo antes de esta tarea:
    tasks:
        - name: Tarea finalizada
        # include_role: (roles a nivel de tarea)
        #   name: mariadb
        ansible.builtin.debug:
            msg: "Se han ejecutado el role con éxito"

Variables de roles 
******************

Las variables de roles se añaden en dos sitios diferentes y tienen mayor o menor precedencia según carpeta:

* Añadimos un par de variables a **defaults/main.yml**:

.. code-block:: yaml 
    :linenos:

    ---
    # defaults file for mariadb
    software: mariadb-server 
    servicio: mariadb

* Hacemos uso de estas variables en las tareas **tasks/main.yml**:

.. code-block:: yaml 
    :linenos:

    ---
    # tasks file for mariadb
    - name: Instalar {{ software }} 
    ansible.builtin.yum:
        name: "{{ software }}"
        state: present

    - name: Arrancar {{ servicio }}
    ansible.builtin.service:
        name: "{{ servicio }}" 
        state: started

Si ejecutamos el playbook ``ansible-playbook pruebabook.yaml`` nos instalará el software que hemos declarado en las variables, maríadb. Para ver la precedencia entre default y vars vamos a crear las mismas variables en vars pero para instalar apache en **vars/main.yml**:

.. code-block:: yaml 
    :linenos:

    ---
    # vars file for mariadb
    software: httpd 
    servicio: httpd

Handlers y ficheros en roles 
****************************

1. Lo primero será crear un archivo dentro del directorio **files** llamado **new_db.sql**:

.. code-block:: sql 
    :linenos:

    CREATE DATABASE prueba_ansible;

2. Vamos a configurar un handler con un comando para crear una base de datos en **handlers/main.yml**:

.. code-block:: yaml 
    :linenos:

    ---
    # handlers file for mariadb
    - name: copiar_script_sql
    ansible.builtin.copy:
        src: files/new_db.sql
        dest: /tmp/new_db.sql

    - name: crear_db
    ansible.builtin.shell:
        cmd: mysql < /tmp/new_db.sql

3. Por último notificamos la invocación de los handlers tras arrancar el servidor en **tasks/main.yml**:

.. code-block:: yaml 
    :linenos:

    ---
    # tasks file for mariadb
    - name: Instalar MariaDB 
    ansible.builtin.yum:
        name: mariadb-server
        state: present

    - name: Arrancar MariaDB
    ansible.builtin.service:
        name: mariadb 
        state: restarted
    notify: # llamamos a dos handlers para la operación
        - copiar_script_sql
        - crear_db

.. attention::
    Los handlers solo se ejecutan cuando las tareas realizan algún tipo de cambio o cualquier cosa que retorne una respuesta. En el caso de started no se estaba ejecutando porque ya estaba iniciado, pero al hacer restarted si devuelve un mensaje y por tanto ejecuta el handler 

pre y post tasks 
****************

Permiten ejecutar tareas dentro del play antes y después del role editamos **pruebabook.yaml**:

.. code-block:: yaml 
    :linenos:

    ---
    - name: Prueba roles
    hosts: hermes 
    become: true 
    become_user: root
    become_method: sudo

    roles:
        - mariadb
    
    # tareas a ejecutar antes del role: 
    pre_tasks:
        - name: pre-tarea
        debug:
            msg: Mensaje antes de ejecutar role


    tasks:
        - name: Tarea finalizada
        ansible.builtin.debug:
            msg: "Se han ejecutado el role con éxito"

    # tareas a ejecutar despues del role y lo que queramos:
    post_tasks:
    - name: post-tarea 
        debug:
        msg: Mensaje despues de ejecutar role y tareas


Tags 
####

Las tags se utilizan para tener un mayor control sobre las tareas. 

Crear tag 
*********

.. code-block:: yaml 
    :linenos:

    ---
    - name: Prueba roles
    hosts: hermes 
    become: true 
    become_user: root
    become_method: sudo

    roles:
        - mariadb
    
    pre_tasks:
        - name: pre-tarea
        debug:
            msg: Mensaje antes de ejecutar role
        tags: # Asignamos un tag a las tareas concretas para una cosa
            - desarrollo


    tasks:
        - name: Tarea finalizada
        ansible.builtin.debug:
            msg: "Se han ejecutado el role con éxito"
        tags: # Asignamos un tag a las tareas concretas para una cosa
            - produccion

    post_tasks:
    - name: post-tarea 
        debug:
        msg: Mensaje despues de ejecutar role y tareas
        tags: # podemos añadir otras tags
        - desarrollo
        - produccion 
        # - never (no ejecutar nunca la tarea)
        # - always (ejecutar siempre la tarea)

* Ahora si queremos ejecutar solo uno de los tags ejecutamos: ``ansible-playbook pruebabook.yaml -t desarrollo``
* Si queremos ejecutar varias tags: ``ansible-playbook pruebabook.yaml -t "desarrollo, produccion"``
  
.. attention::
    Esto ignorará incluso los roles. De modo que para que se ejecute las tareas del role habrá que añadirle el tag que vamos a ejecutar. 


Comandos relacionados con tags  
******************************

* Para saber las tags de un playbook: ``ansible-playbook pruebabook.yaml --list-tags``
* Para saber las tasks de un tag: ``ansible-playbook pruebabook.yaml --list-tasks -t desarrollo``
* Saltar tags: ``ansible-playbook pruebabook.yaml --skip-tags desarrollo``
* Sacar todas las tareas que tengan al menos un tag: ``ansible-playbook pruebabook.yaml --tags tagged``

Plantillas JINJA 2
##################

Jinja 2 es un motor de plantillas que se usa dentro de ansible y otros entornos.

Uso de jinja en ansible 
***********************

1. Creamos un archivo de plantilla llamado **plantilla_prueba.j2**:

.. code-block:: jinja
    :linenos:

    Hola {{ansible_hostname}}
    Hoy es {{ansible_date_time.date}}

2. Para utilizarlo en ansible editamos **plantilla.yaml**:

.. code-block:: yaml 
    :linenos:

    ---
    - name: Trabajar con Jinja 2
    hosts: hermes 

    tasks:
    
    - name: Preparar desarrollo 
        ansible.builtin.template: # ejecutamos template 
        src: plantilla_prueba.j2
        dest: /tmp/salida.txt

3. Ejecutamos ``ansible-playbook plantilla.yaml`` 
4. Ahora leemos el archivo **/tmp/salida.txt** en el servidor remoto veremos que se muestran los datos ofrecidos por las variables.

Condiciones en Jinja 2
**********************

1. Creamos una nueva plantilla llamada **condicionales.j2**:

.. code-block:: jinja 
    :linenos:

    <h1>Pagina web de Ansible</h1>

    {% if ansible_distribution == 'CentOS'%}
        <h2>Creada plantilla para Centos</h2>
    {% else %}
        <h2>No conozco la máquina</h2>
    {% endif %}

2. Ahora se la pasamos a **plantilla.yaml**:

.. code-block:: jinja 
    :linenos:

    ---
    - name: Trabajar con Jinja 2
    hosts: hermes
    become: true 
    become_user: root 
    become_method: sudo

    tasks:
    - name: Averiguar la máquina
        debug:
        msg: "{{ansible_distribution}}"
    - name: Preparar desarrollo 
        ansible.builtin.template: # ejecutamos template 
        src: condicionales.j2
        dest: /var/www/html/index.html

Bucles 
******

1. Creamos una nueva plantilla llamada **bucles.j2**:

.. code-block:: jinja 
    :linenos:

    <h1>Listado de consolas</h1>

    <ul>
        {% for consola in consolas %}
            <li> {{ consola }}</li>
        {% endfor %}
    </ul>


2. Ahora se la pasamos a **plantilla.yaml** y se crea la lista que usará el template:

.. code-block:: jinja 
    :linenos:

    ---
    - name: Trabajar con Jinja 2
    hosts: hermes
    become: true 
    become_user: root 
    become_method: sudo
    # creamos la lista que usará el template: 
    vars:
        consolas:
        - Megadrive
        - Playstation 
        - Gameboy
        - Switch 
        - Steam Deck 

    tasks:
    - name: Averiguar la máquina
        debug:
        msg: "{{ansible_distribution}}"
    - name: Preparar desarrollo 
        ansible.builtin.template: # ejecutamos template 
        src: bucles.j2
        dest: /var/www/html/index.html


Filtros 
*******

1. Modificamos la plantilla llamada **filtros.j2** y el pasamos un filtro a la variable:

.. code-block:: jinja 
    :linenos:

    <h1>Listado de consolas</h1>

    <ul>
        {% for consola in consolas %}
            <li> {{ consola | upper }}</li>
        {% endfor %}
        <li> Lista inline: {{ consolas | join(', ') }} </li>
    </ul>


2. Ahora se la pasamos a **plantilla.yaml** y se crea la lista que usará el template:

.. code-block:: jinja 
    :linenos:

    ---
    - name: Trabajar con Jinja 2
    hosts: hermes
    become: true 
    become_user: root 
    become_method: sudo
    # creamos la lista que usará el template: 
    vars:
        consolas:
        - Megadrive
        - Playstation 
        - Gameboy
        - Switch 
        - Steam Deck 

    tasks:
    - name: Averiguar la máquina
        debug:
        msg: "{{ansible_distribution}}"
    - name: Preparar desarrollo 
        ansible.builtin.template: # ejecutamos template 
        src: bucles.j2
        dest: /var/www/html/index.html

Ansible Vault
#############

Con ansible vault podemos encriptar tanto variables como ficheros y establecer una contraseña para ver la información encriptada.

Encriptar ficheros existentes y visualizar su contenido 
*******************************************************

1. Creamos por ejemplo un archivo **claves.yaml**:

.. code-block:: yaml 
    :linenos:

    pass: prueba
    pass1: prueba1

2. Para encriptarlo ejecutamos: ``ansible-vault encrypt claves.yaml`` e introducimos la clave para cifrar. (1234)
3. Si intentamos ver el archivo estará encriptado. Para poder verlo hay que ejecutar: ``ansible-vault view claves.yaml`` e introducimos la password (1234)

Crear fichero encriptado y editarlo 
***********************************

1. Para crear un fichero encriptado se ejecuta: ``ansible-vault create claves2.yaml`` y se establece la contraseña (1234)
2. Se abre el editor e introducimos valores y guardamos.
3. Para editar un fichero encriptado se ejecuta: ``ansible-vault edit claves2.yaml`` y se establece la contraseña (1234)

.. note::
    Si queremos cambiar nuestro editor por defecto en linux ejecutamos en consola el comando ``export EDITOR=nano`` 

Desencriptar un fichero y cambiar clave 
***************************************

1. Para desencriptar un fichero: ``ansible-vault decrypt claves.yaml`` y ponemos la clave (1234)
2. Para cambiar clave de un fichero: ``ansible-vault rekey claves.yaml`` y ponemos la clave antigua (1234) y luego la nueva (5678)

Usar fichero para pasa password 
*******************************

1. Para no tener que estar añadiendo la clave constantemente creamos un fichero por ejemplo **mi_clave.txt** y escribimos la clave en el.
2. Para utilizar la clave del fichero se la pasamos por comando: ``ansible-vault view claves2.yaml --vault-password-file=mi_clave.txt``

Usar vault en playbooks 
***********************

1. Creamos un playbook y editamos **claves_book.yaml**:

.. code-block:: yaml 
    :linenos:

    --- 
    - name: Prueba de vault 
    hosts: hermes 
    # cargamos las variables de un archivo:
    vars_files: claves2.yaml

    tasks:
    - name: Mostrar contenido encriptado
        debug:
        msg: "{{ prueba }}"

2. para poder utilizar el archivo encriptado habrá que ejecutar el playbook con el siguiente flag: ``ansible-playbook claves_book.yaml --vault-password-file=mi_clave.txt``

