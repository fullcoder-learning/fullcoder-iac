.. Fullcoder documentation master file, created by
   sphinx-quickstart on Wed Jan  6 20:33:17 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Fulldevops - Herramientas para SRE
==================================

.. toctree::
   :maxdepth: 1
   :caption: Integración continua:

   manuals/jenkins

.. toctree::
   :maxdepth: 1
   :caption: Infraestructura como código:

   manuals/ansible
   manuals/terraform

.. toctree::
   :maxdepth: 1
   :caption: Control de versiones:

   manuals/git
   manuals/gitlab 
   manuals/github

.. toctree::
   :maxdepth: 1
   :caption: Supervisión y registro:

   manuals/prometheus
   manuals/grafana
   manuals/cypress

.. toctree::
   :maxdepth: 1
   :caption: Tecnologías del servidor:

   manuals/linux-bash
   manuals/programacion-bash
   manuals/nginx
   manuals/docker

.. toctree::
   :maxdepth: 1
   :caption: Entrega Continua 

   manuals/docker-compose
   manuals/docker-swang
   manuals/kubernetes
   manuals/sonarqube

.. toctree::
   :maxdepth: 1
   :caption: Infraestructuras

   manuals/heroku

.. toctree::
   :maxdepth: 1
   :caption: Herramientas de desarrollo

   manuals/vsc
