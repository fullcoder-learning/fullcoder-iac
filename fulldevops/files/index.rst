.. Fullcoder documentation master file, created by
   sphinx-quickstart on Wed Jan  6 20:33:17 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Fulldevops - Herramientas para SRE
==================================

.. toctree::
   :maxdepth: 1
   :caption: Infraestructura como código:

   manuals/ansible

.. toctree::
   :maxdepth: 1
   :caption: Tecnologías del servidor:

   manuals/linux-bash
   manuals/programacion-bash
   manuals/nginx
   manuals/docker

.. toctree::
   :maxdepth: 1
   :caption: Infraestructuras

   manuals/heroku

.. toctree::
   :maxdepth: 1
   :caption: Herramientas de desarrollo

   manuals/vsc
