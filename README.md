# FULLCODER IAC

Proyecto fullcoder basado en un infraestructura como código con el propósito de automatizar el despliegue de distintos manuales Fullcoder destinados a diferentes lenguajes de programación o propósitos.

## Instalación 

### Instalar Ansible

Vamos al sitio oficial de ansible y lo instalamos en nuestra máquina: ``https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html``

### Preparar el servidor 

1. Instalación limpia de un servidor Ubuntu (22.04 recomendada) 
2. Crear usuario Ansible en el equipo local y en el servidor remoto: ``sudo adduser ansible``
3. En máquina remota ejecutamos ``sudo visudo`` y añadimos la siguiente línea debajo de ``root    ALL=(ALL:ALL) ALL`` : ``ansible ALL=(ALL) NOPASSWD:ALL``
4. Arrancamos el usuario ansible: ``su ansible``
5. Creamos clave ssh: ``ssh-keygen`` y a continuación copiamos clave en servidor remoto ``ssh-copy-id [IP REMOTA]``
6. Creamos nuestro usuario de la máquina local en la máquina remota y repetimos los pasos del 3 al 5.

## Desplegar proyecto

* Despliegue completo (solo la primera vez o si hacemos cambios en Sphinx): ``ansible-playbook deploy.yaml``
* Despliegue de Fulldevops: ``ansible-playbook deploy.yaml -t fulldevops``

## Crear nuevo sitio

1. En la raiz del proyecto creamos un nuevo role ``ansible-galaxy init [nombre_proyecto]``
2. Creamos un archivo **index.rst** y los directorios **manuals** y **logos** dentro del directorio **files**. 
3. Tomamos de referencia el contenido de **tasks/main** de cualquier otro proyecto para crear el playbook.

## Añadir manuales al sitio 

1. Accedemos a uno de los proyectos y dentro de su carpeta **files** es donde vamos a trabajar.
2. En la carpeta logos podemos añadir un logo del manual. 
3. En la carpeta manuals añadimos o modificamos los manuales en formato **rst**. (puedes consultar la documentación del lenguaje de marcado en la url: https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html)
4. Por último añadimos al archivo **index.rst** el nuevo manual. Este index lo podemos encontrar en la misma carpeta **files** del proyecto. 
